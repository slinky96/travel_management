package it.unicas.view;

import it.unicas.util.OperationsDB;
import it.unicas.util.Utenti;
import it.unicas.util.Viaggi;
import it.unicas.util.Voli;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

import java.sql.SQLException;
import java.util.List;

public class ModificaViaggioController {

    String sesso;
    private Stage primaryStage;
    private Viaggi currentViaggio;
    private HomepageController homepageController ;


    public void initModificaViaggiController(Stage stage, Viaggi viaggio,HomepageController homepageController) throws SQLException {

        sesso = "";
        currentViaggio = viaggio;
        primaryStage = stage;
        this.homepageController=homepageController;
        codVolo.setText(currentViaggio.getVoli_num_volo());
        treno.setSelected(currentViaggio.getCondivido_treno());
        auto.setSelected(currentViaggio.getCondivido_auto());
        bus.setSelected(currentViaggio.getCondivido_bus());
        taxi.setSelected(currentViaggio.getCondivido_taxi());
        autoScelta.setSelected(currentViaggio.getOffro_auto());
        etaPartenza.setText(currentViaggio.getEta_start());
        etaArrivo.setText(currentViaggio.getEta_end());
        if (currentViaggio.getSesso_ricercato().equals("M"))
            sessoMaschio.setSelected(true);
        else if (currentViaggio.getSesso_ricercato().equals("F"))
            sessoFemmina.setSelected(true);
        List<Voli> tmp = OperationsDB.getInstance().listaVoli(currentViaggio.getVoli_num_volo());
        compagnia.setText(tmp.get(0).getCompagnia());
        luogoArrivo.setText(tmp.get(0).getDestinazione());
        luogoPartenza.setText(tmp.get(0).getOrigine());
        dataPartenza.setText(tmp.get(0).getData_partenza() + " " + tmp.get(0).getOra_partenza());
        dataArrivo.setText(tmp.get(0).getData_arrivo() + " " + tmp.get(0).getOra_arrivo());
    }

    @FXML
    public  Label codVolo;

    @FXML
    public Label compagnia;

    @FXML
    private Label luogoPartenza;

    @FXML
    private Label luogoArrivo;

    @FXML
    private Label dataPartenza;

    @FXML
    private Label dataArrivo;

    @FXML
    private TextField etaPartenza;

    @FXML
    private RadioButton sessoMaschio;

    @FXML
    private CheckBox auto;

    @FXML
    private CheckBox autoScelta;

    @FXML
    private CheckBox bus;

    @FXML
    private CheckBox treno;

    @FXML
    private CheckBox taxi;

    @FXML
    private RadioButton sessoFemmina;

    @FXML
    private TextField etaArrivo;





    @FXML
    private void handleModifica() throws SQLException {

        System.out.println("OK");

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Campi omessi");
        alert.setHeaderText("Fai attenzione, hai omesso dei campi");
        alert.setContentText("Devi selezionare almeno una preferenza di viaggio");

        Viaggi viaggi = new Viaggi(currentViaggio.getUtenti_email(),codVolo.getText(),selezionaSesso(),etaPartenza.getText(),etaArrivo.getText(),auto.isSelected(),bus.isSelected(),treno.isSelected(),taxi.isSelected(),autoScelta.isSelected());

        //controllo
        if(!(auto.isSelected() || bus.isSelected() || treno.isSelected() || taxi.isSelected() || autoScelta.isSelected()))
            alert.showAndWait();
        else{
            OperationsDB.getInstance().eliminaViaggi(currentViaggio);
            OperationsDB.getInstance().insertViaggi(viaggi);
            System.out.println("Viaggio inserito..");
            homepageController.getStoricoViaggiTableView().getItems().removeAll(currentViaggio);
            homepageController.getStoricoViaggiTableView().getItems().add(viaggi);
            homepageController.getStoricoViaggiTableView().getSortOrder().add(homepageController.getNumVoloColumn());
            //homepageController.loadStoricoViaggi();
            primaryStage.close();
        }
    }


    public String selezionaSesso() {

        if(sessoMaschio.isSelected()== false && sessoFemmina.isSelected()==true) {

            //non selezionato

            return sesso="F";
        }
           else if (sessoMaschio.isSelected()== true && sessoFemmina.isSelected()==false) {

        return sesso="M";
            }
           else{

               return sesso;
        }

    }
}
