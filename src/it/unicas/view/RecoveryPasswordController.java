package it.unicas.view;

import it.unicas.util.EmailUtility;
import it.unicas.util.OperationsDB;
import it.unicas.util.Utenti;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.mail.MessagingException;
import java.sql.SQLException;
import java.util.Random;

public class RecoveryPasswordController {

    private Stage stage;

    @FXML
    private Label messageLabel;

    @FXML
    private TextField emailField;

    public void setDialogStage(Stage stage){

        this.stage = stage;
    }



    @FXML
    private void handleClose(){

        stage.close();
    }

    @FXML
    private void handleRecuperaPassword() throws SQLException {

        System.out.println(emailField.getText());

        if(emailField.getText().isEmpty()){

            if (emailField.getText().isEmpty())
                emailField.setStyle("-fx-base: rgb(217,100,101);");

            messageLabel.setText("Compilare correttamente il campo email");
        }
        else{

            Utenti utente_tmp = new Utenti();
            utente_tmp.setEmail(emailField.getText());

            if (OperationsDB.getInstance().controlUtente(utente_tmp, false)) {

                utente_tmp = OperationsDB.getInstance().selectUtente(utente_tmp, false).get(0);
                utente_tmp.setPassword(getRandomString(8));

                OperationsDB.getInstance().updateUtente(utente_tmp);

                String object = "Recupero password Travel Management";
                String message = "\n\nCome da te richiesto ti stiamo inviano la nuova password'\n\nLa tua nuova password e': " + utente_tmp.getPassword() +
                        "\n\nPrendi nota della tua nuova password e non divulgarla\n";

                try {
                    EmailUtility.sendEmail("smtp.gmail.com", "587", "travelmanagement2019@gmail.com", "travel2019", utente_tmp.getEmail(), object, message);
                } catch (MessagingException ex){
                    System.out.println("Error send mail");
                }

                messageLabel.setStyle("-fx-text-fill: rgb(0,130,0)");
                messageLabel.setText("Nuova password inviata all'indirizzo " + emailField.getText());
            }
            else{

                messageLabel.setStyle("-fx-text-fill: rgb(255,0,0)");
                messageLabel.setText("L'email inserita non e' associata a nessun account");
            }
        }

    }

    public static String getRandomString(int length) {

        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST1234567890".toCharArray();

        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String randomStr = sb.toString();

        return randomStr;
    }
}
