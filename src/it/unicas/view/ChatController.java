package it.unicas.view;

import it.unicas.util.Chat;
import it.unicas.util.OperationsDB;
import it.unicas.util.Utenti;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.Timer;

public class ChatController implements ActionListener {

    private Stage stage;
    private Utenti currentUser;
    private Utenti otherUser;
    private List<Chat> oldMessage;

    public void initChatController(Stage stage, Utenti current, Utenti other) throws SQLException {

        this.stage = stage;
        currentUser = current;
        otherUser = other;

        titleChatField.setText("Chat with " + otherUser.getNome());

        Timer timer = new Timer(1000, this);
        timer.setInitialDelay(500);
        timer.start();

        this.stage.setOnCloseRequest( event ->
        {
            timer.stop();
            stage.close();
        });

        loadOldMessage();
    }

    @FXML
    private Label titleChatField;

    @FXML
    private TextFlow oldMessageArea;

    @FXML
    private TextArea newMessageArea;

    @FXML
    private ScrollPane verticalScrollPane;

    public void loadOldMessage() throws SQLException {

        oldMessage = OperationsDB.getInstance().listaChat(currentUser.getEmail(), otherUser.getEmail());

        if(oldMessageArea.getChildren().size()/2 != oldMessage.size()) {

            ArrayList<Text> heading = new ArrayList<>();
            ArrayList<Text> message = new ArrayList<>();

            oldMessageArea.getChildren().clear();

            for (int i = 0; i < oldMessage.size(); i++) {

                if (oldMessage.get(i).getUtenteMittente().equals(currentUser.getEmail())){
                    heading.add(new Text(oldMessage.get(i).getDataMessaggio() + " " + oldMessage.get(i).getOraMessaggio() + " hai scritto:\n"));
                    heading.get(i).setFill(Color.BLUE);
                }
                else{
                    heading.add(new Text(oldMessage.get(i).getDataMessaggio() + " " + oldMessage.get(i).getOraMessaggio() + " " + otherUser.getNome() + " ha scritto:\n"));
                    heading.get(i).setFill(Color.DARKGREEN);
                }
                message.add(new Text(oldMessage.get(i).getMessaggio() + "\n"));

                oldMessageArea.getChildren().add(heading.get(i));
                oldMessageArea.getChildren().add(message.get(i));
            }

            verticalScrollPane.setVvalue(1.0);
        }
    }

    @FXML
    private void handleSend() throws SQLException {

        if(!newMessageArea.getText().isEmpty()){
            GregorianCalendar dataOraAttuale=new GregorianCalendar();
            String dataMessaggio = dataOraAttuale.get(GregorianCalendar.DATE) + "/" + dataOraAttuale.get(GregorianCalendar.MONTH) + "/" + dataOraAttuale.get(GregorianCalendar.YEAR);
            String oraMessaggio  = ((dataOraAttuale.get(GregorianCalendar.HOUR_OF_DAY) <= 9) ? ("0" + dataOraAttuale.get(GregorianCalendar.HOUR_OF_DAY)) : dataOraAttuale.get(GregorianCalendar.HOUR_OF_DAY)) + ":" +
                    ((dataOraAttuale.get(GregorianCalendar.MINUTE) <= 9) ? ("0" + dataOraAttuale.get(GregorianCalendar.MINUTE)) : dataOraAttuale.get(GregorianCalendar.MINUTE));

            Chat c = new Chat(newMessageArea.getText(), dataMessaggio, oraMessaggio, currentUser.getEmail(), otherUser.getEmail());
            OperationsDB.getInstance().insertChat(c);

            newMessageArea.clear();
            loadOldMessage();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Platform.runLater(() -> {
            try {
                loadOldMessage();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }
}
