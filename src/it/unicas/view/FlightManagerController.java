package it.unicas.view;

import it.unicas.util.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.controlsfx.control.textfield.TextFields;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class FlightManagerController {

    private Stage primaryStage;
    private Utenti currentUser;
    private String sesso;
    private String possibleWords[];
    private HomepageController homepageController ;


    public void initFlightManagerController(Stage stage, Utenti utente,HomepageController homepageController) throws SQLException {

        possibleWords = OperationsDB.getInstance().listaVoli().toArray(new String[0]);
        sesso = "";
        currentUser = utente;
        primaryStage = stage;
        this.homepageController=homepageController;


    }

    @FXML
    public  TextField codVolo;

    @FXML
    public Label compagnia;

    @FXML
    private Label luogoPartenza;

    @FXML
    private Label luogoArrivo;

    @FXML
    private Label dataPartenza;

    @FXML
    private Label dataArrivo;

    @FXML
    private TextField etaPartenza;

    @FXML
    private RadioButton sessoMaschio;

    @FXML
    private CheckBox auto;

    @FXML
    private CheckBox autoScelta;

    @FXML
    private CheckBox bus;

    @FXML
    private CheckBox treno;

    @FXML
    private CheckBox taxi;

    @FXML
    private RadioButton sessoFemmina;

    @FXML
    private TextField etaArrivo;





    @FXML
    private void handleInserisci() throws SQLException {

        System.out.println("OK");

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Campi omessi");
        alert.setHeaderText("Fai attenzione, hai omesso dei campi");
        alert.setContentText("Devi selezionare almeno una preferenza di viaggio");

        Alert alert1 = new Alert(Alert.AlertType.ERROR);
        alert1.setTitle("Volo già inserito");
        alert1.setHeaderText("Stai cercando di aggiungere preferenze per un volo già inserito");
        alert1.setContentText("Modifica da \"Storico Viaggi\"");

        Alert alert2 = new Alert(Alert.AlertType.ERROR);
        alert2.setTitle("Codice Volo");
        alert2.setHeaderText("Codice volo inesistente");
        alert2.setContentText("Continua");

        boolean control=true;
        List<Voli> x = OperationsDB.getInstance().listaVoli(codVolo.getText());

        List<Viaggi> a = OperationsDB.getInstance().listaViaggi(currentUser.getEmail());

        for (int i=0; i<a.size(); i++) {
            if (a.get(i).getVoli_num_volo().equals(codVolo.getText()))
                control = false;
        }

        Viaggi viaggi = new Viaggi(currentUser.getEmail(),codVolo.getText(),selezionaSesso(),etaPartenza.getText(),etaArrivo.getText(),auto.isSelected(),bus.isSelected(),treno.isSelected(),taxi.isSelected(),autoScelta.isSelected());

        //controllo
        if(!(auto.isSelected() || bus.isSelected() || treno.isSelected() || taxi.isSelected() || autoScelta.isSelected()))
            alert.showAndWait();
        else if (!control)
            alert1.showAndWait();
        else if (x.isEmpty())
            alert2.showAndWait();
        else{
            OperationsDB.getInstance().insertViaggi(viaggi);
          homepageController.getStoricoViaggiTableView().getItems().add(viaggi);
          homepageController.getStoricoViaggiTableView().getSortOrder().add(homepageController.getNumVoloColumn());
            System.out.println("Viaggio inserito..");

            primaryStage.close();
        }
    }

    @FXML
    public void handleCercaVolo() throws SQLException {

        String num_volo=codVolo.getText();
        List<Voli> a = OperationsDB.getInstance().listaVoli(num_volo);
        if (a.isEmpty()){
            luogoPartenza.setText("");
            luogoArrivo.setText("");
            compagnia.setText("");
            dataPartenza.setText("");
            dataArrivo.setText("");

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText("Numero volo non esistente");
            alert.setContentText("Continua");

            alert.showAndWait();
        }
        else {
            luogoPartenza.setText(a.get(0).getOrigine());
            luogoArrivo.setText(a.get(0).getDestinazione());
            dataPartenza.setText(a.get(0).getData_partenza() + " " + a.get(0).getOra_partenza());
            dataArrivo.setText(a.get(0).getData_arrivo() + " " + a.get(0).getOra_arrivo());
            compagnia.setText((a.get(0).getCompagnia()));
        }
    }

    public String selezionaSesso() {

        if(sessoMaschio.isSelected()== false && sessoFemmina.isSelected()==true) {

            //non selezionato

            return sesso="F";
        }
           else if (sessoMaschio.isSelected()== true && sessoFemmina.isSelected()==false) {

        return sesso="M";
            }
           else{

               return sesso;
        }

    }
    public void auto_fill(){
            TextFields.bindAutoCompletion(codVolo,possibleWords);
    }
}
