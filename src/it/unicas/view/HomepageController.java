package it.unicas.view;

import com.sun.xml.internal.org.jvnet.mimepull.MIMEMessage;
import it.unicas.MainApp;
import it.unicas.util.CoViaggiatori;
import it.unicas.util.Utenti;
import it.unicas.util.Viaggi;
import it.unicas.util.OperationsDB;
import it.unicas.util.DateUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.io.File;

public class HomepageController {

    private MainApp mainApp;
    private Stage primaryStage;
    private AnchorPane rootLayout;

    private Utenti currentUser;
    private String emailOtherUser;
    private Viaggi currentViaggi;
    private boolean cameraVisible;

    private ObservableList<Viaggi> viaggiData = FXCollections.observableArrayList();
    private ObservableList<CoViaggiatori> coViaggiatoriData = FXCollections.observableArrayList();


    public void initHomepageController(MainApp mainApp, Utenti utente) throws Exception {

        this.mainApp = mainApp;
        primaryStage = mainApp.getPrimaryStage();
        primaryStage.centerOnScreen();
        primaryStage.setTitle("Travel Management");
        rootLayout = mainApp.getRootLayout();
        currentUser = utente;
        cameraVisible = false;
        modificaButton.setTooltip(new Tooltip("Modifica dati personali"));
        confermaButton.setTooltip(new Tooltip("Conferma modifiche ai dati personali"));
        modificaViaggi.setTooltip(new Tooltip("Modifica le preferenze del viaggio selezionato"));
        eliminaButton.setTooltip(new Tooltip("Elimina viaggio selezionato"));
        filtroButton.setTooltip(new Tooltip("attiva o disattiva filtro preferenze per il viaggio selezionato"));
        BackButton.setTooltip(new Tooltip("Torna alla schermata di Login"));
        chatButton.setTooltip(new Tooltip("Apre la chat con l'utente selezionato o l'ultimo contattato"));
        travelButton.setTooltip(new Tooltip("Apre schermata per l'inserimento di un viaggio"));

        startUserArea();
        loadStoricoViaggi();
    }

    @FXML
    private Button travelButton;

    @FXML
    private Button BackButton;

    @FXML
    private MenuItem eliminaViaggio;

    @FXML
    private MenuItem modificaViaggio;

    @FXML
    private Button modificaButton;

    @FXML
    private Button modificaViaggi;

    @FXML
    private Button eliminaButton;

    @FXML
    private ToggleButton filtroButton;

    @FXML
    private Button confermaButton;

    @FXML
    private Button chatButton;

    @FXML
    private Circle avatar;

    @FXML
    private ImageView cameraId;

    @FXML
    private TextField emailField;

    @FXML
    private TextField nomeField;

    @FXML
    private TextField cognomeField;

    @FXML
    private TextField telefonoField;

    @FXML
    private RadioButton sessoButtonM;

    @FXML
    private RadioButton sessoButtonF;

    @FXML
    private TextField datanascitaField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField passwordTextField;


    @FXML
    private TableView<Viaggi> storicoViaggiTableView;

    @FXML
    private TableColumn<Viaggi, String> numVoloColumn;

    @FXML
    private TableColumn<Viaggi, String> sessoColumn;

    @FXML
    private TableColumn<Viaggi, String> etaStartColumn;

    @FXML
    private TableColumn<Viaggi, String> etaEndColumn;

    @FXML
    private TableColumn<Viaggi, Boolean> cAutoColumn;

    @FXML
    private TableColumn<Viaggi, Boolean> cBusColumn;

    @FXML
    private TableColumn<Viaggi, Boolean> cTrenoColumn;

    @FXML
    private TableColumn<Viaggi, Boolean> cTaxiColumn;

    @FXML
    private TableColumn<Viaggi, Boolean> oAutoColumn;


    @FXML
    private TableView<CoViaggiatori> coViaggiatoriTableView;

    @FXML
    private TableColumn<CoViaggiatori, String> nomeColumn;

    @FXML
    private TableColumn<CoViaggiatori,String> etaColumn;

    @FXML
    private TableColumn<CoViaggiatori, String> sessoCVColumn;

    @FXML
    private TableColumn<CoViaggiatori, String> condivideColumn;

    @FXML
    private TableColumn<CoViaggiatori, String> offreColumn;

    public void startUserArea() {

        File file;
        if(currentUser.getUrl_avatar().equals("/src/it/unicas/image/user-default-gray.png"))
            file = new File((System.getProperty("user.dir") + currentUser.getUrl_avatar()));
        else
            file = new File(currentUser.getUrl_avatar());
        if(file.exists() && !file.isDirectory()) {
            Image avatarImage = new Image(file.toURI().toString());
            avatar.setFill(new ImagePattern(avatarImage));
        }
        else
            avatar.setFill(Color.GRAY);

        emailField.setText(currentUser.getEmail());
        nomeField.setText(currentUser.getNome());
        cognomeField.setText(currentUser.getCognome());
        telefonoField.setText(currentUser.getTelefono());
        if (currentUser.getSesso().equals("M"))
            sessoButtonM.setSelected(true);
        else if (currentUser.getSesso().equals("F"))
            sessoButtonF.setSelected(true);
        datanascitaField.setText(currentUser.getData_nascita());
        passwordField.setText(currentUser.getPassword());
    }

    public void loadStoricoViaggi() throws Exception {

        storicoViaggiTableView.setItems(viaggiData);
        numVoloColumn.setCellValueFactory(cellData -> cellData.getValue().voli_num_voloProperty());
        sessoColumn.setCellValueFactory(cellData -> cellData.getValue().sesso_ricercatoProperty());
        etaStartColumn.setCellValueFactory(cellData -> cellData.getValue().eta_startProperty());
        etaEndColumn.setCellValueFactory(cellData -> cellData.getValue().eta_endProperty());
        cAutoColumn.setCellValueFactory(cellData -> cellData.getValue().condivido_autoProperty());
        cBusColumn.setCellValueFactory(cellData -> cellData.getValue().condivido_busProperty());
        cTrenoColumn.setCellValueFactory(cellData -> cellData.getValue().condivido_trenoProperty());
        cTaxiColumn.setCellValueFactory(cellData -> cellData.getValue().condivido_taxiProperty());
        oAutoColumn.setCellValueFactory(cellData -> cellData.getValue().offro_autoProperty());

        List<Viaggi> lista = OperationsDB.getInstance().listaViaggi(currentUser.getEmail());

        for (int i = 0; i < lista.size(); i++)
            storicoViaggiTableView.getItems().add(lista.get(i));

        storicoViaggiTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    try {
                        currentViaggi = newValue;
                        showCoViaggiatori(newValue);
                    } catch (Exception e) {
                        currentViaggi = null;
                    }
                });

    }

    public void showCoViaggiatori(Viaggi v) throws SQLException, Exception {

        coViaggiatoriTableView.setItems(coViaggiatoriData);
        nomeColumn.setCellValueFactory(cellData -> cellData.getValue().getNomeProperty());
        etaColumn.setCellValueFactory(cellData -> cellData.getValue().getEtaProperty());
        sessoCVColumn.setCellValueFactory(cellData -> cellData.getValue().getSessoProperty());
        condivideColumn.setCellValueFactory(cellData -> cellData.getValue().getCondivideProperty());
        offreColumn.setCellValueFactory(cellData -> cellData.getValue().getOffreProperty());

        List<CoViaggiatori> lista;

        if(coViaggiatoriTableView.getItems().size() != 0)
            coViaggiatoriTableView.getItems().clear();

        if(filtroButton.isSelected())
            lista = OperationsDB.getInstance().listaCoViaggiatori(v, currentUser.getSesso(), false);
        else
            lista = OperationsDB.getInstance().listaCoViaggiatori(v, currentUser.getSesso(), true);

        for(int i = 0; i < lista.size(); i++)
            coViaggiatoriTableView.getItems().add(lista.get(i));

        coViaggiatoriTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> emailOtherUser = newValue.getEmail());
    }

    @FXML
    private void handleModifica() {

        modificaButton.setDisable(true);
        confermaButton.setDisable(false);

        nomeField.setEditable(true);
        cognomeField.setEditable(true);
        telefonoField.setEditable(true);
        sessoButtonM.setDisable(false);
        sessoButtonF.setDisable(false);
        datanascitaField.setEditable(true);
        passwordField.setEditable(true);

        //set color modifier
        nomeField.setStyle("-fx-base: rgb(232,191,90);");
        cognomeField.setStyle("-fx-base: rgb(232,191,90);");
        telefonoField.setStyle("-fx-base: rgb(232,191,90);");
        datanascitaField.setStyle("-fx-base: rgb(232,191,90);");
        passwordField.setStyle("-fx-base: rgb(232,191,90);");
    }

    @FXML
    private void handleModificaViaggi() throws SQLException, IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/ModificaViaggio.fxml"));
        rootLayout = loader.load();

        Scene scene = new Scene(rootLayout);
        scene.setRoot(rootLayout);

        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Modifica Viaggio");
        stage.initModality((Modality.WINDOW_MODAL));
        stage.initOwner(primaryStage);

        ModificaViaggioController controller = loader.getController();
        controller.initModificaViaggiController(stage, currentViaggi,this);

        stage.showAndWait();

    }

    @FXML
    private void handleConferma() throws SQLException {

        String sesso = "";

        if (nomeField.getText().length() >= 45)
            nomeField.setText(nomeField.getText(0, 44));
        if (cognomeField.getText().length() >= 45)
            cognomeField.setText(cognomeField.getText(0, 44));
        if (telefonoField.getText().length() == 45)
            telefonoField.setText(telefonoField.getText(0, 44));

        currentUser.setNome(nomeField.getText());
        currentUser.setCognome(cognomeField.getText());
        currentUser.setTelefono(telefonoField.getText());
        if (sessoButtonM.isSelected())
            sesso = "M";
        else if (sessoButtonF.isSelected())
            sesso = "F";
        currentUser.setSesso(sesso);
        if(DateUtil.checkDate(datanascitaField.getText()))
            currentUser.setData_nascita(datanascitaField.getText());
        else if (datanascitaField.getText().isEmpty())
            currentUser.setData_nascita("");
        else {
            datanascitaField.setStyle("-fx-base: rgb(217,100,101);");
            return;
        }
        currentUser.setPassword(passwordField.getText());

        nomeField.setStyle("-fx-base: rgb(255,255,255);");
        cognomeField.setStyle("-fx-base: rgb(255,255,255);");
        telefonoField.setStyle("-fx-base: rgb(255,255,255);");
        datanascitaField.setStyle("-fx-base: rgb(255,255,255);");
        passwordField.setStyle("-fx-base: rgb(255,255,255);");

        OperationsDB.getInstance().updateUtente(currentUser);

        nomeField.setEditable(false);
        cognomeField.setEditable(false);
        telefonoField.setEditable(false);
        sessoButtonM.setDisable(true);
        sessoButtonF.setDisable(true);
        datanascitaField.setEditable(false);
        passwordField.setEditable(false);

        confermaButton.setDisable(true);
        modificaButton.setDisable(false);
    }

    @FXML
    private void handleBack() throws IOException {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Sei sicuro di voler disconnetterti");
        alert.setContentText("Conferma");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/Login.fxml"));
            rootLayout = loader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            LoginController controller = loader.getController();
            controller.initLoginController(mainApp);

            System.out.println("Ritorno alla schermata di Login");
        }
    }

    @FXML
    private void handleTravel() throws IOException, SQLException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/FlightManager.fxml"));
        rootLayout = loader.load();

        Scene scene = new Scene(rootLayout);
        scene.setRoot(rootLayout);

        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Flight Manager");
        stage.initModality((Modality.WINDOW_MODAL));
        stage.initOwner(primaryStage);

        FlightManagerController controller = loader.getController();
        controller.initFlightManagerController(stage, currentUser,this);

        stage.showAndWait();
    }

    @FXML
    private void openChat() throws IOException, SQLException {

        if(emailOtherUser != null){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/Chat.fxml"));
            rootLayout = loader.load();

            Scene scene = new Scene(rootLayout);
            scene.setRoot(rootLayout);

            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Flight Manager - Chat");
            stage.initModality((Modality.WINDOW_MODAL));
            stage.initOwner(primaryStage);

            Utenti otherUser = new Utenti();
            otherUser.setEmail(emailOtherUser);

            ChatController controller = loader.getController();
            controller.initChatController(stage, currentUser, OperationsDB.getInstance().selectUtente(otherUser, false).get(0));

            stage.showAndWait();
        }
    }

    @FXML
    public void showCamera() {
        if (!cameraVisible) {
            cameraId.setVisible(true);
            cameraVisible = true;
        } else {
            cameraId.setVisible(false);
            cameraVisible = false;
        }
    }

    @FXML
    public void changeAvatar() throws SQLException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a new avatar image");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files (.png, .jpg)", "*.png", "*.jpg"));
        File selectedFile = fileChooser.showOpenDialog(primaryStage);
        if (selectedFile != null) {
            currentUser.setUrl_avatar(selectedFile.toString());
            OperationsDB.getInstance().updateUtente(currentUser);
            startUserArea();
        }
    }

    @FXML
    private void handleSessoM() {
        if (sessoButtonF.isSelected())
            sessoButtonF.setSelected(false);
    }

    @FXML
    private void handleSessoF() {
        if (sessoButtonM.isSelected())
            sessoButtonM.setSelected(false);
    }

    @FXML
    private void handlePasswordText() {
        passwordField.setStyle("-fx-base: rgb(255,255,255);");
        if (passwordField.getText().length() == 45) {
            passwordField.setText(passwordField.getText(0, 44));
            passwordField.setStyle("-fx-base: rgb(217,100,101);");
        }
    }

    @FXML
    private void handlePasswordView() {
        passwordTextField.setText(passwordField.getText());
        passwordField.setVisible(false);
    }

    @FXML
    private void handlePasswordHide() {
        passwordTextField.setText(" ");
        passwordField.setVisible(true);
    }

    @FXML
    public void handleEliminaViaggio() throws SQLException {

        eliminaButton.setDisable(false);
        OperationsDB.getInstance().eliminaViaggi(currentViaggi);
        storicoViaggiTableView.getItems().removeAll(currentViaggi);
    }

    @FXML
    private void clearTableSelection() {

        if(storicoViaggiTableView.getSelectionModel().isEmpty()){
            eliminaViaggio.setDisable(true);
            eliminaButton.setDisable(true);
            modificaViaggio.setDisable(true);
            modificaViaggi.setDisable(true);
            //chatButton.setDisable(true);
        }
        else {
            modificaViaggi.setDisable(false);
            eliminaButton.setDisable(false);
            modificaViaggio.setDisable(false);
            eliminaViaggio.setDisable(false);
            //chatButton.setDisable(false);
        }
    }

    @FXML
    private void handleAbout() throws IOException {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setTitle("About us");
        alert.setHeaderText("Flight Manager");
        String intext ="Applicazione ideata da: \n" +
                "\n" +
                "Cantone Marco \n" +
                "\n" +
                "Messina Davide \n" +
                "\n" +
                "Pugliese Nicola \n";

        File directory =new  File( (System.getProperty("user.dir")+  "/src/it/unicas/image/logo.png"));
        if(directory.exists() && !directory.isDirectory()){
            Image image = new Image(directory.toURI().toString());

            ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(image);

        }

        alert.setContentText(intext);
        alert.show();
    }

    @FXML
    public void handleFiltro() throws Exception {
        showCoViaggiatori(currentViaggi);
    }

    public TableView<Viaggi> getStoricoViaggiTableView(){ return storicoViaggiTableView; }
    public TableColumn<Viaggi,String> getNumVoloColumn(){ return numVoloColumn; }
}