package it.unicas.view;

import it.unicas.util.EmailUtility;
import it.unicas.MainApp;
import it.unicas.util.OperationsDB;
import it.unicas.util.Utenti;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.SQLException;

public class SignUpController {

    private MainApp mainApp;
    private Stage primaryStage;
    private AnchorPane rootLayout;

    public void initSignUpController(MainApp mainApp){

        this.mainApp = mainApp;
        primaryStage = this.mainApp.getPrimaryStage();
        primaryStage.setTitle("Travel Management - Sign Up");
        rootLayout   = this.mainApp.getRootLayout();
    }

    @FXML
    private TextField emailField;

    @FXML
    private  TextField nomeField;

    @FXML
    private TextField cognomeField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private PasswordField passwordConfirmField;

    @FXML
    private TextField passwordTextField;

    @FXML
    private TextField passwordConfirmTextField;

    @FXML
    private Label messageLabel;

    @FXML
    private void handleSignIn() throws SQLException {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Attenzione");

        Utenti utente_tmp = new Utenti();
        utente_tmp.setEmail(emailField.getText());

        if (emailField.getText().isEmpty() || nomeField.getText().isEmpty() || cognomeField.getText().isEmpty() || passwordField.getText().isEmpty()){
            alert.setHeaderText("Campi obbligatori omessi");
            alert.setContentText("Inserire tutti i campi");
            alert.showAndWait();
        }
        else if (!(emailField.getText().matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"))){
            alert.setHeaderText("Email errata");
            alert.setContentText("L'email deve essere un formato valido");
            alert.showAndWait();
        }
        else if (!(passwordField.getText().equals(passwordConfirmField.getText()))){
            alert.setHeaderText("Password non coincidenti");
            alert.setContentText("I campi 'password' e 'conferma password' devono essere uguali");
            alert.showAndWait();
        }
        else if (OperationsDB.getInstance().controlUtente(utente_tmp, false)){
            alert.setHeaderText("Utente già registrato");
            alert.setContentText("Effettuare il login o recuperare la password con la procedura apposita");
            alert.showAndWait();
        }
        else{
            utente_tmp = new Utenti(emailField.getText(), passwordField.getText(), nomeField.getText(), cognomeField.getText(), "", "", "/src/it/unicas/image/user-default-gray.png", "");
            System.out.println("Invio email a " + utente_tmp.getEmail());

            String object = "Benvenuto in Travel Management G7/2019";
            String message = "\n\nGrazie per esserti registrato al nostro portale viaggi,\neffettua al piu' presto l'accesso e sfrutta tutte le funzionalita'\n";

            try {

                EmailUtility.sendEmail("smtp.gmail.com", "587", "travelmanagement2019@gmail.com", "travel2019", utente_tmp.getEmail(), object, message);

                System.out.println("Email inviata a " + utente_tmp.getEmail());
                OperationsDB.getInstance().insertUtente(utente_tmp);
                handleBack();
            } catch (MessagingException | IOException ex) {
                messageLabel.setText("Registrazione non effettuata. Invio dell'email non riuscito");
                OperationsDB.getInstance().insertUtente(utente_tmp);
            }
        }
        }
    /*
    @FXML
    private void handleSignIn() throws SQLException, IOException {

        Utenti utente_tmp;

        if(emailField.getText().isEmpty() || nomeField.getText().isEmpty() || cognomeField.getText().isEmpty() || passwordField.getText().isEmpty()){
            if (emailField.getText().isEmpty())
                emailField.setStyle("-fx-base: rgb(217,100,101);");
            if (nomeField.getText().isEmpty())
                nomeField.setStyle("-fx-base: rgb(217,100,101);");
            if (cognomeField.getText().isEmpty())
                cognomeField.setStyle("-fx-base: rgb(217,100,101);");
            if (passwordField.getText().isEmpty())
                passwordField.setStyle("-fx-base: rgb(217,100,101);");

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Campi obbligatori omessi");
            alert.setContentText("Inserire tutti i campi");

            ButtonType buttonTypeOk = new ButtonType("Ok", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOk);
            alert.showAndWait();

            System.out.println("Messaggio di alert per campi obbligatori omessi");
        }
        else if(passwordField.getText().equals(passwordConfirmField.getText())) {

            if(emailField.getText().matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$")) {

                utente_tmp = new Utenti(emailField.getText(), passwordField.getText(), nomeField.getText(), cognomeField.getText(), "", "", "/src/it/unicas/image/user-default-gray.png", "");
                System.out.println("Invio email a " + utente_tmp.getEmail());

                String object = "Benvenuto in Travel Management G7/2019";
                String message = "\n\nGrazie per esserti registrato al nostro portale viaggi,\neffettua al piu' presto l'accesso e sfrutta tutte le funzionalita'\n";

                try {

                    EmailUtility.sendEmail("smtp.gmail.com", "587", "travelmanagement2019@gmail.com", "travel2019", utente_tmp.getEmail(), object, message);

                    System.out.println("Email inviata a " + utente_tmp.getEmail());
                    OperationsDB.getInstance().insertUtente(utente_tmp);
                    handleBack();
                } catch (MessagingException ex) {
                    messageLabel.setText("Registrazione non effettuata. Invio dell'email non riuscito");
                    OperationsDB.getInstance().insertUtente(utente_tmp);
                }
            }

            else{

                emailField.setStyle("-fx-base: rgb(217,100,101);");

                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Attenzione");
                alert.setHeaderText("L'email inserita non e' in un formato valido");
                alert.setContentText("Controlla l'email che hai inserito e riporva ad effettuare il Sign In");

                ButtonType buttonTypeOk = new ButtonType("Ok", ButtonBar.ButtonData.CANCEL_CLOSE);
                alert.getButtonTypes().setAll(buttonTypeOk);
                alert.showAndWait();

                System.out.println("Messaggio di alert per formato email errato");
            }
        }
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Attenzione");
            alert.setHeaderText("Errore nell'inserimento della password");
            alert.setContentText("I campi password e conferma password non coincidono");

            ButtonType buttonTypeOk = new ButtonType("Ok", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOk);
            alert.showAndWait();

            System.out.println("Messaggio di alert per password non coincidente");
        }
    }
    */

    @FXML
    private void handleBack() throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/Login.fxml"));
        rootLayout = loader.load();

        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);

        LoginController controller = loader.getController();
        controller.initLoginController(mainApp);

        System.out.println("Ritorno alla schermata di Login");
    }

    @FXML
    private void handleEmailText(){

        emailField.setStyle("-fx-base: rgb(255,255,255);");
        if (emailField.getText().length() == 45) {
            emailField.setText(emailField.getText(0, 44));
            emailField.setStyle("-fx-base: rgb(217,100,101);");
        }
    }

    @FXML
    private void handleNomeText(){

        nomeField.setStyle("-fx-base: rgb(255,255,255);");
        if (nomeField.getText().length() == 45) {
            nomeField.setText(nomeField.getText(0, 44));
            nomeField.setStyle("-fx-base: rgb(217,100,101);");
        }
    }

    @FXML
    private void handleCognomeText(){

        cognomeField.setStyle("-fx-base: rgb(255,255,255);");
        if (cognomeField.getText().length() == 45) {
            cognomeField.setText(cognomeField.getText(0, 44));
            cognomeField.setStyle("-fx-base: rgb(217,100,101);");
        }
    }

    @FXML
    private void handlePasswordText(){

        passwordField.setStyle("-fx-base: rgb(255,255,255);");
        if (passwordField.getText().length() == 45) {
            passwordField.setText(passwordField.getText(0, 44));
            passwordField.setStyle("-fx-base: rgb(217,100,101);");
        }
    }

    @FXML
    private void handlePasswordView(){

        passwordTextField.setText(passwordField.getText());
        passwordConfirmTextField.setText(passwordConfirmField.getText());
        passwordField.setVisible(false);
        passwordConfirmField.setVisible(false);
    }

    @FXML
    private void handlePasswordHide(){

        passwordTextField.setText("");
        passwordConfirmTextField.setText("");
        passwordField.setVisible(true);
        passwordConfirmField.setVisible(true);
    }
}