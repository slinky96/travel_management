package it.unicas.view;

import it.unicas.MainApp;
import it.unicas.util.OperationsDB;
import it.unicas.util.Utenti;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class LoginController {

    private MainApp mainApp;
    private Stage primaryStage;
    private AnchorPane rootLayout;

    @FXML
    private TextField emailField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField passwordTextField;

    @FXML
    private Label messageField;


    public void initLoginController(MainApp mainApp){

        this.mainApp = mainApp;
        primaryStage = this.mainApp.getPrimaryStage();
        primaryStage.centerOnScreen();
        primaryStage.setTitle("Travel Management - Login");



        File iconDirectory = new File((System.getProperty("user.dir") + "/src/it/unicas/image/logo.png"));
        if(iconDirectory.exists() && !iconDirectory.isDirectory()){
            Image iconImage = new Image(iconDirectory.toURI().toString());
            primaryStage.getIcons().add(iconImage);
        }

        rootLayout   = this.mainApp.getRootLayout();
    }

    @FXML
    public void handleLogin() throws Exception {

        if (emailField.getText().isEmpty() || passwordField.getText().isEmpty()){

            if (emailField.getText().isEmpty())
                emailField.setStyle("-fx-base: rgb(217,100,101);");
            if (passwordField.getText().isEmpty())
                passwordField.setStyle("-fx-base: rgb(217,100,101);");

            messageField.setText("Compilare tutti i campi richiesti");
        }
        else {

            Utenti utente_tmp = new Utenti();
            utente_tmp.setEmail(emailField.getText());
            utente_tmp.setPassword(passwordField.getText());

            if (OperationsDB.getInstance().controlUtente(utente_tmp, true)) {

                messageField.setText("Accesso effettuato correttamente :)");
                messageField.setStyle("-fx-text-fill: rgb(0,255,0)");

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("view/Homepage2.fxml"));

                rootLayout = loader.load();

                Scene scene = new Scene(rootLayout);
                primaryStage.setScene(scene);

                HomepageController controller = loader.getController();
                utente_tmp = OperationsDB.getInstance().selectUtente(utente_tmp, true).get(0);
                controller.initHomepageController(mainApp, utente_tmp);
            }
            else
                messageField.setText("Email o password errata");
        }
    }

    @FXML
    private void handleSignUp() throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/SignUp.fxml"));
        rootLayout = loader.load();

        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);

        SignUpController controller = loader.getController();
        controller.initSignUpController(mainApp);

        System.out.println("Pulsante SignUp cliccato. Cambio schermata");
    }

    @FXML
    private void handlePasswordView(){

        passwordTextField.setText(passwordField.getText());
        passwordField.setVisible(false);
    }

    @FXML
    private void handlePasswordHide(){
        passwordTextField.setText("");
        passwordField.setVisible(true);
    }

    @FXML
    private void handlePasswordDimenticata() throws IOException {

        System.out.println("Apertura schermata per recupero password");

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/RecoveryPassword.fxml"));
        rootLayout = loader.load();

        Scene scene = new Scene(rootLayout);
        scene.setRoot(rootLayout);

        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Travel Management - Recovery Password");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        stage.setResizable(false);

        RecoveryPasswordController controller = loader.getController();
        controller.setDialogStage(stage);

        stage.show();
    }
}
