package it.unicas;

import it.unicas.view.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

import it.unicas.util.OperationsDB;
import javafx.stage.WindowEvent;

public class MainApp extends Application {

    private Stage primaryStage;
    private AnchorPane rootLayout;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        OperationsDB.getInstance().setParameters("remotemysql.com:3306", "w6TlHCcTqo", "qUIMvgBorg", "w6TlHCcTqo");

        this.primaryStage = primaryStage;

        initLoginLayout();

        this.primaryStage.show();
    }

    public void initLoginLayout() throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("view/Login.fxml"));
        rootLayout = loader.load();

        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        //set the information about the closing window
        primaryStage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::closeWindowEvent);
        LoginController controller = loader.getController();
        controller.initLoginController(this);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public AnchorPane getRootLayout() {
        return rootLayout;
    }

    private void closeWindowEvent(WindowEvent event) {

        System.out.println("Window close request ...");

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.getButtonTypes().remove(ButtonType.OK);
        alert.getButtonTypes().add(ButtonType.CANCEL);
        alert.getButtonTypes().add(ButtonType.YES);
        alert.setTitle("Chiusura Flight Manager");
        alert.setContentText(String.format("Sei sicuro di  voler uscire da Flight Manager?"));
        alert.initOwner(primaryStage.getOwner());
        Optional<ButtonType> res = alert.showAndWait();

        if (res.isPresent()) {
            if (res.get().equals(ButtonType.CANCEL))
                event.consume();
        }

    }
}