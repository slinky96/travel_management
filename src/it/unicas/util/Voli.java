package it.unicas.util;

public class Voli {
    private String num_volo;
    private String compagnia;
    private String origine;
    private String data_partenza;
    private String ora_partenza;
    private String destinazione;
    private String data_arrivo;
    private String ora_arrivo;

    public Voli(){
        num_volo="";
        compagnia="";
        origine="";
        data_partenza="";
        ora_partenza="";
        destinazione="";
        data_arrivo="";
        ora_arrivo="";
    }

    public  Voli(String num_volo, String compagnia, String origine, String data_partenza, String ora_partenza, String destinazione, String data_arrivo, String ora_arrivo){
        this.num_volo=num_volo;
        this.compagnia=compagnia;
        this.origine=origine;
        this.data_partenza=data_partenza;
        this.ora_partenza=ora_partenza;
        this.destinazione=destinazione;
        this.data_arrivo=data_arrivo;
        this.ora_arrivo=ora_arrivo;
    }

    public String getNum_volo(){ return num_volo;}
    public String getCompagnia(){return compagnia;}
    public String getOrigine(){return origine;}
    public String getData_partenza(){return data_partenza;}
    public String getOra_partenza(){return ora_partenza;}
    public String getDestinazione(){return destinazione;}
    public String getData_arrivo(){return data_arrivo;}
    public String getOra_arrivo(){return ora_arrivo;}

    public void  setNum_volo(String a){num_volo=a;}
    public void  setcompagnia(String a){compagnia=a;}
    public void  setOrigine(String a){origine=a;}
    public void  setData_partenza(String a){data_partenza=a;}
    public void  setOra_partenza(String a){ora_partenza=a;}
    public void  setDestinazione(String a){destinazione=a;}
    public void  setData_arrivo(String a){data_arrivo=a;}
    public void  setOra_arrivo(String a){ora_arrivo=a;}

}
