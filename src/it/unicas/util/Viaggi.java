package it.unicas.util;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Viaggi {

    private StringProperty utenti_email;
    private StringProperty voli_num_volo;
    private StringProperty sesso_ricercato;
    private StringProperty eta_start;
    private StringProperty eta_end;
    private BooleanProperty condivido_auto;
    private BooleanProperty condivido_bus;
    private BooleanProperty condivido_treno;
    private BooleanProperty condivido_taxi;
    private BooleanProperty offro_auto;


    public Viaggi(){

        utenti_email = new SimpleStringProperty("");
        voli_num_volo = new SimpleStringProperty("");
        sesso_ricercato = new SimpleStringProperty("");
        eta_start = new SimpleStringProperty("");
        eta_end = new SimpleStringProperty("");
        condivido_auto = new SimpleBooleanProperty(false);
        condivido_bus = new SimpleBooleanProperty(false);
        condivido_treno = new SimpleBooleanProperty(false);
        condivido_taxi = new SimpleBooleanProperty(false);
        offro_auto = new SimpleBooleanProperty(false);
    }



    public Viaggi(String utenti_email, String voli_num_volo, String sesso_ricercato, String eta_start, String eta_end, boolean condivido_auto, boolean condivido_bus, boolean condivido_treno, boolean condivido_taxi, boolean offro_auto){

        this.utenti_email = new SimpleStringProperty(utenti_email);
        this.voli_num_volo = new SimpleStringProperty(voli_num_volo);
        this.sesso_ricercato = new SimpleStringProperty(sesso_ricercato);
        this.eta_start = new SimpleStringProperty(eta_start);
        this.eta_end = new SimpleStringProperty(eta_end);
        this.condivido_auto = new SimpleBooleanProperty(condivido_auto);
        this.condivido_bus = new SimpleBooleanProperty(condivido_bus);
        this.condivido_treno = new SimpleBooleanProperty(condivido_treno);
        this.condivido_taxi = new SimpleBooleanProperty(condivido_taxi);
        this.offro_auto = new SimpleBooleanProperty(offro_auto);
    }

    public String getUtenti_email() {return utenti_email.get();}
    public String getVoli_num_volo() {return voli_num_volo.get();}
    public String getSesso_ricercato() {return sesso_ricercato.get();}
    public String getEta_start() {return eta_start.get();}
    public String getEta_end() {return eta_end.get();}
    public boolean getCondivido_auto() {return condivido_auto.get();}
    public boolean getCondivido_bus() {return condivido_bus.get();}
    public boolean getCondivido_treno() {return condivido_treno.get();}
    public boolean getCondivido_taxi() {return condivido_taxi.get();}
    public boolean getOffro_auto() {return offro_auto.get();}

    public void setUtenti_email(String value) {utenti_email.set(value);}
    public void setVoli_num_volo(String value) {voli_num_volo.set(value);}
    public void setSesso_ricercato(String value) {sesso_ricercato.set(value);}
    public void setEta_start(String value) {eta_start.set(value);}
    public void setEta_end(String value) {eta_end.set(value);}
    public void setCondivido_auto(boolean value) {condivido_auto.set(value);}
    public void setCondivido_bus(boolean value) {condivido_bus.set(value);}
    public void setCondivido_treno(boolean value) {condivido_treno.set(value);}
    public void setCondivido_taxi(boolean value) {condivido_taxi.set(value);}
    public void setOffro_auto(boolean value) {offro_auto.set(value);}

    public StringProperty utenti_emailProperty() {return utenti_email;}
    public StringProperty voli_num_voloProperty() {return voli_num_volo;}
    public StringProperty sesso_ricercatoProperty() {return sesso_ricercato;}
    public StringProperty eta_startProperty() {return eta_start;}
    public StringProperty eta_endProperty() {return eta_end;}
    public BooleanProperty condivido_autoProperty() {return condivido_auto;}
    public BooleanProperty condivido_busProperty() {return condivido_bus;}
    public BooleanProperty condivido_trenoProperty() {return condivido_treno;}
    public BooleanProperty condivido_taxiProperty() {return condivido_taxi;}
    public BooleanProperty offro_autoProperty() {return offro_auto;}

}
