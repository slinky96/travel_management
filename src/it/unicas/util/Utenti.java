package it.unicas.util;

public class Utenti {

    private String email;
    private String nome;
    private String cognome;
    private String telefono;
    private String password;
    private String sesso;
    private String url_avatar;
    private String data_nascita;

    public Utenti(){
        email = "";
        nome = "";
        cognome = "";
        telefono = "";
        password = "";
        sesso = "";
        url_avatar = "";
        data_nascita = "";
    }

    public Utenti(String email, String password, String nome, String cognome, String telefono, String sesso, String url_avatar, String data_nascita){

        this.email = email;
        this.nome = nome;
        this.cognome = cognome;
        this.telefono = telefono;
        this.password = password;
        this.sesso = sesso;
        this.url_avatar = url_avatar;
        this.data_nascita = data_nascita;
    }


    //GETTER
    public String getEmail() {
        return email;
    }
    public String getNome() {
        return nome;
    }
    public String getCognome() {
        return cognome;
    }
    public String getTelefono() {
        return telefono;
    }
    public String getPassword() {
        return password;
    }
    public String getSesso() {
        return sesso;
    }
    public String getUrl_avatar() {return url_avatar;}
    public String getData_nascita() {return data_nascita;}

    //SETTER
    public void setEmail(String email) {
        this.email = email;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setSesso(String sesso) {
        this.sesso = sesso;
    }
    public void setUrl_avatar(String url_avatar) {this.url_avatar = url_avatar;}
    public void setData_nascita(String data_nascita) {this.data_nascita = data_nascita;}
}
