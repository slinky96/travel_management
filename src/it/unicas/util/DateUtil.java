package it.unicas.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {

    public static boolean checkDate(final String dt1)
    {
        SimpleDateFormat df1=new SimpleDateFormat("yyyy-MM-dd");
        df1.setLenient(false);
        try {
            df1.parse(dt1);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /*
    public int getEta(String birth){
        int giorno = Integer.parseInt(birth.substring(0,1));
        int mese = Integer.parseInt(birth.substring(3,4));
        int anno = Integer.parseInt(birth.substring(6,9));
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.of(anno, mese, giorno);
        Period p = Period.between(birthday, today);
        return p.getYears();
    }
    */
}