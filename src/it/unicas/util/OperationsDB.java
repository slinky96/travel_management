package it.unicas.util;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class OperationsDB {

    private final static String PARAMETERS = "?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    private static OperationsDB istance = null;
    private static Connection connection;
    private static Statement statement;

    private OperationsDB(){}

    public static OperationsDB getInstance() {

        if (istance ==null)
            istance = new OperationsDB();
        return istance;
    }

    public void setParameters(String host, String username, String password, String schema) throws SQLException {
        
        connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + schema + PARAMETERS, username, password);
        statement  = connection.createStatement();
    }

    public List<Voli> listaVoli(String a) throws SQLException {

        ArrayList<Voli> lista = new ArrayList<>();
        String query = "SELECT * FROM voli WHERE num_volo like '" + a + "'";
        ResultSet rs = statement.executeQuery(query);
        while(rs.next()){
            lista.add(new Voli(rs.getString("num_volo"),
                    rs.getString("compagnia"),
                    rs.getString("origine"),
                    rs.getString("data_partenza"),
                    rs.getString("ora_partenza"),
                    rs.getString("destinazione"),
                    rs.getString("data_arrivo"),
                    rs.getString("ora_arrivo")));
        }
        return lista;
    }

    public Vector<String> listaVoli() throws SQLException {
        Vector<String> a = new Vector<>();
        String query = "SELECT * FROM voli";
        ResultSet rs = statement.executeQuery(query);
        while(rs.next())
            a.add(rs.getString("num_volo"));

        return a;
    }

    public List<CoViaggiatori> listaCoViaggiatori(Viaggi v, String mySesso, boolean generic) throws SQLException{
        ArrayList<CoViaggiatori> lista =  new ArrayList<>();
        String query = "";
        if(!generic) {
            query = "SELECT viaggi.utenti_email, utenti.nome, FLOOR(DATEDIFF(NOW(), utenti.data_nascita)/365) AS eta, utenti.sesso, viaggi.condivido_auto, " +
                    "viaggi.condivido_bus, viaggi.condivido_treno, viaggi.condivido_taxi, viaggi.offro_auto FROM viaggi JOIN utenti ON viaggi.utenti_email=utenti.email " +
                    "WHERE viaggi.utenti_email<>'" + v.getUtenti_email() + "' AND viaggi.voli_num_volo='" + v.getVoli_num_volo() + "' AND FLOOR(DATEDIFF(NOW(), utenti.data_nascita)/365)>='" +
                    v.getEta_start() + "' AND FLOOR(DATEDIFF(NOW(), utenti.data_nascita)/365)<='" + v.getEta_end() + "' AND (((viaggi.condivido_bus=" + v.getCondivido_bus() + " OR viaggi.condivido_treno=" +
                    v.getCondivido_treno() + ") OR viaggi.condivido_taxi=" + v.getCondivido_taxi() + ") OR viaggi.offro_auto=" + v.getCondivido_auto() + ") AND (utenti.sesso='" + v.getSesso_ricercato() +
                    "' AND viaggi.sesso_ricercato='" + mySesso + "')";
        }
        else{
            query = "SELECT viaggi.utenti_email, utenti.nome, FLOOR(DATEDIFF(NOW(), utenti.data_nascita)/365) AS eta, utenti.sesso, viaggi.condivido_auto, " +
                    "viaggi.condivido_bus, viaggi.condivido_treno, viaggi.condivido_taxi, viaggi.offro_auto FROM viaggi JOIN utenti ON viaggi.utenti_email=utenti.email " +
                    "WHERE viaggi.utenti_email<>'" + v.getUtenti_email() + "' AND viaggi.voli_num_volo='" + v.getVoli_num_volo() + "' AND viaggi.sesso_ricercato='" + mySesso + "'";
        }
        ResultSet rs = statement.executeQuery(query);
        while(rs.next()){
            lista.add(new CoViaggiatori(rs.getString(1), rs.getString(2), rs.getString(3),
                    rs.getString(4),  rs.getBoolean(5), rs.getBoolean(6),
                    rs.getBoolean(7), rs.getBoolean(8), rs.getBoolean(9)));
        }

        return lista;
    }

    public List<Viaggi> listaViaggi(String email) throws SQLException {
        ArrayList<Viaggi> lista =  new ArrayList<>();
        String query = "SELECT * FROM viaggi WHERE utenti_email like'" + email +"'";
        ResultSet rs = statement.executeQuery(query);
        while(rs.next())
           lista.add(new Viaggi(rs.getString(1),rs.getString(2), rs.getString(3),
                    rs.getString(4),rs.getString(5), rs.getBoolean(6),
                    rs.getBoolean(7),rs.getBoolean(8), rs.getBoolean(9),
                    rs.getBoolean(10) ) );

        return lista;
    }

    public List<Chat> listaChat(String emailMittente, String emailDestinatario) throws SQLException {
        ArrayList<Chat> lista = new ArrayList<>();

        String query = "SELECT idChat, messaggio, dataMessaggio, oraMessaggio, utenteMittente, utenteDestinatario FROM chat WHERE " +
                "utenteMittente='" + emailMittente + "' AND utenteDestinatario='" + emailDestinatario + "' UNION " +
                "SELECT idChat, messaggio, dataMessaggio, oraMessaggio, utenteMittente, utenteDestinatario FROM chat WHERE " +
                "utenteDestinatario='" + emailMittente + "' AND utenteMittente='" + emailDestinatario + "' ORDER BY idChat";

        ResultSet rs = statement.executeQuery(query);
        while(rs.next())
            lista.add(new Chat(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)));

        return lista;
    }

    public List<Utenti> selectUtente(Utenti a, boolean exact_psw) throws SQLException {
        ArrayList<Utenti> lista = new ArrayList<>();
        String query = "SELECT * FROM utenti WHERE email like '" + a.getEmail() + "' and password like ";
        if (exact_psw)
            query += "'" + a.getPassword() + "'";
        else
            query = query + "'%" + a.getPassword() + "%'";
        query += " and nome like '%" + a.getNome() +
                "%' and cognome like '%" + a.getCognome() +
                "%' and telefono like '%" + a.getTelefono() +
                "%' and sesso like '%" + a.getSesso() +
                "%' and url_avatar like '%" + a.getUrl_avatar() +
                "%' and data_nascita like '%" + a.getData_nascita() + "%'";
        //System.out.println(query);
        ResultSet rs = statement.executeQuery(query);
        while(rs.next()){
            lista.add(new Utenti(rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("nome"),
                    rs.getString("cognome"),
                    rs.getString("telefono"),
                    rs.getString("sesso"),
                    rs.getString("url_avatar"),
                    rs.getString("data_nascita")));
        }
        return lista;
    }

    public boolean controlUtente(Utenti a, boolean exact_psw) throws SQLException {
        List<Utenti> x = selectUtente(a, exact_psw);
        return !x.isEmpty();
    }

    public void updateUtente(Utenti a) throws SQLException {
        String query = "UPDATE utenti set password=?, nome=?, cognome=?, telefono=?, sesso=?, url_avatar=?, data_nascita=? WHERE email=?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, a.getPassword());
        ps.setString(2, a.getNome());
        ps.setString(3, a.getCognome());
        ps.setString(4, a.getTelefono());
        ps.setString(5, a.getSesso());
        ps.setString(6, a.getUrl_avatar());
        ps.setString(7, a.getData_nascita());
        ps.setString(8, a.getEmail());

        ps.executeUpdate();
    }

    public void insertUtente(Utenti a) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO utenti (email, password, nome, cognome, telefono, sesso, url_avatar, data_nascita) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        ps.setString(1, a.getEmail());
        ps.setString(2, a.getPassword());
        ps.setString(3, a.getNome());
        ps.setString(4, a.getCognome());
        ps.setString(5, a.getTelefono());
        ps.setString(6, a.getSesso());
        ps.setString(7, a.getUrl_avatar());
        ps.setString(8, a.getData_nascita());

        ps.executeUpdate();
    }

    public void eliminaViaggi(Viaggi v) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(
                "DELETE FROM viaggi WHERE utenti_email = '" + v.getUtenti_email() + "' AND voli_num_volo ='" + v.getVoli_num_volo() +
                        "' AND sesso_ricercato='" + v.getSesso_ricercato() + "' AND eta_start='" + v.getEta_start() + "' AND eta_end='" + v.getEta_end() +
                        "' AND condivido_auto=" + v.getCondivido_auto() + " AND condivido_bus=" + v.getCondivido_bus() +
                        " AND condivido_treno=" + v.getCondivido_treno() + " AND condivido_taxi=" + v.getCondivido_taxi() +
                        " AND offro_auto=" + v.getOffro_auto() + " LIMIT 1");

        ps.executeUpdate();
    }

    public void insertViaggi(Viaggi v) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO viaggi (utenti_email, voli_num_volo, sesso_ricercato, eta_start, eta_end, condivido_auto, condivido_bus, condivido_treno, condivido_taxi, offro_auto) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        ps.setString(1, v.getUtenti_email());
        ps.setString(2, v.getVoli_num_volo());
        ps.setString(3, v.getSesso_ricercato());
        ps.setString(4, v.getEta_start());
        ps.setString(5, v.getEta_end());
        ps.setBoolean(6, v.getCondivido_auto());
        ps.setBoolean(7, v.getCondivido_bus());
        ps.setBoolean(8, v.getCondivido_treno());
        ps.setBoolean(9, v.getCondivido_taxi());
        ps.setBoolean(10, v.getOffro_auto());

        ps.executeUpdate();
    }

    public void insertChat(Chat c) throws SQLException{
        PreparedStatement ps = connection.prepareStatement(
                "INSERT INTO chat (messaggio, dataMessaggio, oraMessaggio, utenteMittente, utenteDestinatario) VALUES (?,?,?,?,?)");
        ps.setString(1, c.getMessaggio());
        ps.setString(2, c.getDataMessaggio());
        ps.setString(3, c.getOraMessaggio());
        ps.setString(4, c.getUtenteMittente());
        ps.setString(5, c.getUtenteDestinatario());

        ps.executeUpdate();
    }
}
