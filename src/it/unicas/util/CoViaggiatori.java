package it.unicas.util;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CoViaggiatori {

    private StringProperty email;
    private StringProperty nome;
    private StringProperty sesso;
    private StringProperty eta;
    private StringProperty condivide;
    private StringProperty offre;

    public CoViaggiatori(String email , String nome, String eta , String sesso , Boolean cAuto , Boolean cBus , Boolean cTreno ,  Boolean cTaxi , Boolean oAuto){

        this.email=new SimpleStringProperty(email);
        this.nome=new SimpleStringProperty(nome);
        this.eta=new SimpleStringProperty(eta);
        this.sesso=new SimpleStringProperty(sesso);

        condivide = new SimpleStringProperty("");
        offre = new SimpleStringProperty("");
        if(cAuto)
            condivide.setValue(" Auto");
        if(cBus)
            condivide.setValue(condivide.getValue()+" Bus");
        if(cTreno)
            condivide.setValue(condivide.getValue()+ " Treno");
        if(cTaxi)
            condivide.setValue(condivide.getValue()+" Taxi");
        if(oAuto)
            offre.setValue("SI");

    }

    public String getEmail() { return email.get(); }

    public StringProperty getNomeProperty() {
        return nome;
    }
    public StringProperty getEtaProperty() { return eta; }
    public StringProperty getSessoProperty() { return sesso; }
    public StringProperty getCondivideProperty() { return condivide; }
    public StringProperty getOffreProperty() { return offre; }

}
