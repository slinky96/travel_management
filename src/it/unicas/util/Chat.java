package it.unicas.util;

public class Chat {

    //private int idChat;
    private String messaggio;
    private String dataMessaggio;
    private String oraMessaggio;
    private String utenteMittente;
    private String utenteDestinatario;

    public Chat(String messaggio, String dataMessaggio, String oraMessaggio, String utenteMittente, String utenteDestinatario){
        //this.idChat = idChat;
        this.messaggio = messaggio;
        this.dataMessaggio = dataMessaggio;
        this.oraMessaggio = oraMessaggio;
        this.utenteMittente = utenteMittente;
        this.utenteDestinatario = utenteDestinatario;
    }

    //public int getIdChat(){ return idChat; }
    public String getMessaggio(){ return messaggio; }
    public String getDataMessaggio(){ return dataMessaggio; }
    public String getOraMessaggio(){ return oraMessaggio; }
    public String getUtenteMittente(){ return utenteMittente; }
    public String getUtenteDestinatario(){ return utenteDestinatario; }

    //public void setIdChat(int idChat){ this.idChat = idChat; }
    public void setMessaggio(String messaggio){ this.messaggio = messaggio; }
    public void setDataMessaggio(String dataMessaggio){ this.dataMessaggio = dataMessaggio; }
    public void setOraMessaggio(String oraMessaggio){ this.oraMessaggio = oraMessaggio; }
    public void setUtenteMittente(String utenteMittente){ this.utenteMittente = utenteMittente; }
    public void setUtenteDestinatario(String utenteDestinatario){ this.utenteDestinatario = utenteDestinatario; }
}
